<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Adminlogin extends CI_Controller {

	var $data = array();

	public function __construct(){
       parent::__construct();		
		$models = array('mcommon', 'madminlogin','mmanagertype','musers');
		foreach($models as $model){
			$this->load->model($model);
		}
		$this->load->library('common_functions');
	}


	public function index()
	{

		if($this->input->is_ajax_request())
		{
			
			$this->form_validation->set_error_delimiters('', '');
			$this->form_validation->set_rules('emailId', 'Email Id', 'required|valid_email|trim');
			$this->form_validation->set_rules('mobileno', 'mobileno', 'required|exact_length[10]');
			$this->form_validation->set_rules('firstName', 'first Name', 'required');
			$this->form_validation->set_rules('lastName', 'mobileno', 'required|trim');
			$this->form_validation->set_rules('password', 'mobileno', 'required');
			$this->form_validation->set_rules('fk_roletypeId', 'fk_roletypeId', 'required|integer');
			if($this->form_validation->run() == FALSE)
			{
				$output=array('flag'=>3,'emailId'=>form_error('emailId'),'mobileno'=>form_error('mobileno'),'firstName'=>form_error('firstName'),'lastName'=>form_error('lastName'),'fk_roletypeId'=>form_error('fk_roletypeId'),'smsg'=>'');
				echo json_encode($output);
			}
			else
			{
				$emailId= $this->input->post('emailId');
				$mobileno= $this->input->post('mobileno');
				$fk_roletypeId= $this->input->post('fk_roletypeId');
				$lastName= $this->input->post('lastName');
				$firstName= $this->input->post('firstName');
				$password= $this->input->post('password');

				$users = $this->musers->read(array('emailId'=>$emailId,'mobileno'=>$mobileno),'row');
				if($users)
				{
		

				 $output=array('flag'=>2,'emsg'=>'Token already Generate Successfully','smsg'=>'','redirect'=>'');

				}
				else
				{

				 
				$data_array = array('emailId'=>$emailId,'mobileno'=>$mobileno,'api_token'=>rand(111111,999999),'token_status'=>1,'firstName'=>$firstName,'lastName'=>$lastName,'createDt'=>date('Y-m-d'),'fk_roletypeId'=>$fk_roletypeId,'password'=>$password);
				$this->mcommon->master_insert($data_array,'users');	

				$output=array('flag'=>1,'smsg'=>'Token Generate Successfully','redirect'=>1);


				}

				echo json_encode($output);			
			}	
		}
		else
		{
			$this->data['type'] = $this->mmanagertype->read(array(),'result');
			$this->load->view('login',$this->data);
		}
	}



























}
