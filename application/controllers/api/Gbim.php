<?php
require(APPPATH.'/libraries/REST_Controller.php');

class Gbim extends REST_Controller{
	var $data = array();
	var $token;
	var $check_token;
	var $Authorization_token;
	
	function __construct(){
		parent::__construct();
		$models = array('mcommon', 'musers');
		foreach($models as $model){
			$this->load->model($model);
		}
		$this->load->library('check_token');
		
		if(array_key_exists("Authorization",apache_request_headers()))
		{
			$Authorization_token = apache_request_headers()['Authorization'];
		}
		else
		{
			$Authorization_token = '';
		}

		$this->Authorization_token =$Authorization_token;
		
		$check_token = $this->check_token = $this->check_token->check(array('token'=>$Authorization_token));  
		if($check_token['flag']==2)
		{
			$this->data['flags'] = $this->check_token;
			$this->response($this->data, 200);
		}
		
		
		//$this->check_token = $this->check_token->check(apache_request_headers()['Authorization']);
		//$this->output->enable_profiler(TRUE);	
  
	}
	

	function user_details_post(){

		if($this->check_token['flag']==1)
		{

			$this->form_validation->set_error_delimiters('', '');
			$this->form_validation->set_rules('fk_roletypeId','fk_roletypeId','required|trim|numeric'); 
			if($this->form_validation->run()==FALSE)
			{
				$this->data['flags']=array('flag'=>3,'emsg'=>'','smsg'=>'','fk_roletypeId'=>form_error('fk_roletypeId'),'validation_errors'=>validation_errors());
			}
			else
			{ 
				$fk_roletypeId = $this->post('fk_roletypeId');
				$mobileno  = $this->post('mobileno');
				$users = $this->musers->read(array('fk_roletypeId'=>$fk_roletypeId),'result'); 
				
				if($users)
				{
					$this->data['flags']=array('flag'=>1, 'smsg'=>'Success', 'emsg'=>'');
					$this->data['users_data']=$users;
					
				}
				else
				{
					$this->data['flags']=array('flag'=>2, 'smsg'=>'', 'emsg'=>'User Details not found');
					$this->data['users_data']=$users;
				}
			}

		}
		else
		{
			$this->data['data']=$check_token;

		}	
			 
		$this->response($this->data, 200); 	
	}

	function user_details_by_date_post(){

		if($this->check_token['flag']==1)
		{

			$this->form_validation->set_error_delimiters('', '');
			$this->form_validation->set_rules('date','date','required'); 
			if($this->form_validation->run()==FALSE)
			{
				$this->data['flags']=array('flag'=>3,'emsg'=>'','smsg'=>'','date'=>form_error('date'),'mobileno'=>form_error('mobileno'), 'validation_errors'=>validation_errors());
			}
			else
			{ 
				$date = $this->post('date');
				$users = $this->musers->read(array('createDt'=>$date),'result'); 
				
				if($users)
				{
					$this->data['flags']=array('flag'=>1, 'smsg'=>'Success', 'emsg'=>'');
					$this->data['users_data']=$users;
					
				}
				else
				{
					$this->data['flags']=array('flag'=>2, 'smsg'=>'', 'emsg'=>'User Details not found');
					$this->data['users_data']=$users;
				}
			}

		}
		else
		{
			$this->data['data']=$check_token;

		}	
			 
		$this->response($this->data, 200); 	
	}


	function user_details_update_post(){

		if($this->check_token['flag']==1)
		{

			$this->form_validation->set_error_delimiters('', '');
			$this->form_validation->set_rules('where_emailId','emailId','required|trim|valid_email'); 
			$this->form_validation->set_rules('mobileno','mobileno','required|trim|numeric|exact_length[10]');  
			$this->form_validation->set_rules('firstName','firstName','required|trim|alpha');  
			$this->form_validation->set_rules('lastName','lastName','required|trim|alpha');  
			if($this->form_validation->run()==FALSE)
			{
				$this->data['flags']=array('flag'=>3,'emsg'=>'','smsg'=>'','where_emailId'=>form_error('where_emailId'),'mobileno'=>form_error('mobileno'),'firstName'=>form_error('firstName'),'lastName'=>form_error('lastName'), 'validation_errors'=>validation_errors());
			}
			else
			{ 
				$emailId = $this->post('where_emailId');
				$mobileno  = $this->post('mobileno');
				$firstName  = $this->post('firstName');
				$lastName  = $this->post('lastName');
				$fk_api_token  = $this->Authorization_token;
				$users = $this->musers->read(array('emailId'=>$emailId),'row'); 
				
				if($users)
				{
					$update_array = array('mobileno'=>$mobileno,'firstName'=>$firstName,'lastName'=>$lastName,'fk_api_token'=>$this->Authorization_token);
					$whereArr=array('emailId'=>$emailId);
					$this->mcommon->master_update('users',$update_array,$whereArr);
					$this->data['flags']		=	array('flag'=>1, 'smsg'=>'Updated Successfully', 'emsg'=>'');
					$users1 = $this->musers->read(array('emailId'=>$emailId),'row'); 
				
					$this->data['users_data']	=   $users1;
					
				}
				else
				{
					$this->data['flags']=array('flag'=>2, 'smsg'=>'', 'emsg'=>'User Details not found');
					$this->data['users_data']=$users;
				}
			}

		}
		else
		{
			$this->data['data']=$check_token;

		}	
			 
		$this->response($this->data, 200); 	
	}

	function user_details_delete_post(){

		if($this->check_token['flag']==1)
		{

			$this->form_validation->set_error_delimiters('', '');
			$this->form_validation->set_rules('emailId','emailId','required|trim|valid_email'); 
			if($this->form_validation->run()==FALSE)
			{
				$this->data['flags']=array('flag'=>3,'emsg'=>'','smsg'=>'','emailId'=>form_error('emailId'),'validation_errors'=>validation_errors());
			}
			else
			{ 
				$emailId = $this->post('emailId');
				$users = $this->musers->read(array('emailId'=>$emailId),'row'); 
				
				if($users)
				{
					$whereArr=array('emailId'=>$emailId);
					$this->mcommon->master_delete($whereArr,'users');

					$this->data['flags']=array('flag'=>1, 'smsg'=>'Deleted Successfully', 'emsg'=>'');
					
				}
				else
				{
					$this->data['flags']=array('flag'=>2, 'smsg'=>'', 'emsg'=>'User Details not found');
					$this->data['users_data']=$users;
				}
			}

		}
		else
		{
			$this->data['data']=$check_token;

		}	
			 
		$this->response($this->data, 200); 	
	}


}

?>