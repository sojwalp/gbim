<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 
 */
class Check_auth_token{
	private $data = array();
	private $CI; 
	public function __construct(){
		$this->CI =& get_instance(); 
		
		$models = array('mapi_tokens');
		foreach($models as $model){
			$this->CI->load->model($model);
		}
	}
	// array('token'=>$token,'fk_userId'=>$userId
	function check($param=array()){
		 
		if($param['token'] !='')
		{
			$check_token = $this->CI->mapi_tokens->read(array('token'=>$param['token'],  'tokenStatus'=>1),'row');
			if($check_token)
			{
				$output = array('flag'=>1, 'emsg'=>'', 'smsg'=>'', 'fk_userId'=>$check_token['fk_userId']);
			}
			else
			{
				$output = array('flag'=>2, 'emsg'=>'Unathorized request,please relogin');
			}
		}
		else
		{
			$output = array('flag'=>2, 'emsg'=>'Request token missing');
		}
		return $output;
	}
	 
	
}
