<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 
 */
class Check_token{
	private $data = array();
	private $CI; 
	public function __construct(){
		$this->CI =& get_instance(); 
		
		$models = array('musers');
		foreach($models as $model){
			$this->CI->load->model($model);
		}
	}
	function check($token=array()){
		 
		if($token['token']!='')
		{
			$check_token = $this->CI->musers->read(array('api_token'=>$token['token'], 'token_status'=>1),'row');
		
			if($check_token)
			{
				$output = array('flag'=>1, 'emsg'=>'', 'smsg'=>'');
			}
			else
			{
				$output = array('flag'=>2, 'emsg'=>'Unathorized request');
			}
		}
		else
		{
			$output = array('flag'=>2, 'emsg'=>'Request token missing');
		}
		return $output;
	}
	 
	
}
