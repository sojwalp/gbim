<?php

defined('BASEPATH') OR exit('No direct script access allowed');
 
class Common_functions  {
	private $data = array();
	private $CI; 
	private $common_model_object; 
	public function __construct(){
		$this->CI =& get_instance(); 
		$models = array('mcommon');
		foreach($models as $model){
			$this->CI->load->model($model);
		} 
		
		$this->common_model_object = $this->CI->mcommon; 
	}
	
	 
	public function mail_common($to='', $subject='', $msgBody='', $userName){
		if($to)
		{
			$headers = "From: noreply@coconetwork.in" . "\r\n" .
			"CC: virendra.patil@eyemagnett.com";

			mail($to,$subject,$msgBody,$headers);
			
			$insert_array = array('toEmail' => $to, 'toName' => $userName, 'fromEmail' =>'noreply@coconetwork.in', 'fromName' =>'coconetwork', 'replyTo' =>'', 'subject' =>$subject,
			'msgBody' =>$msgBody,'sentStatus' =>1,'insertDateTime' =>date('Y-m-d H:i:s'));	
			$this->CI->mcommon->master_insert($insert_array, 'email_notifications');  
		}
	}
	
	public function send_sms($receiver_mobileNo, $sms){
		$log_sms_insert_array = array('receiver_mobileNo' => $receiver_mobileNo, 'sms'=>$sms, 'smsDT' =>date('Y-m-d H:i:s'),'sms_from'=>'sabsafe');
		$master_insert=$this->CI->mcommon->master_insert($log_sms_insert_array, 'log_sms');
		if(ENVIRONMENT == 'production')
		{
			send_sms_single($receiver_mobileNo, $sms);
		}
	}
	 
	public function forgetpasswordmail($toEmail,$mdcode,$userName, $userType){
		$subject="Account recovery";
		$this->data["title"]=$subject;
		 
		$verifyemaillink=base_url()."authentication/resetpassword/".$mdcode;
		 
		$mailcontent["userName"]=$userName;
		$mailcontent["useremailid"]=$toEmail;
		$mailcontent["verifyemaillink"]=$verifyemaillink;
		$mailcontent["unsubscribelink"]="";
		$this->data["mailcontent"]=$mailcontent;
		
		$this->data["content"]='mail/forgotpassword';
		$msgBody = $this->CI->load->view('mail/layout_main',$this->data,TRUE);
		
		$toEmail=$toEmail;

		$sendquickmail = array('toEmail'=>$toEmail,'subject'=>$subject,'msgBody'=>$msgBody,'attachment'=>'');
		$sentStatus = sendquickmail($sendquickmail);	
		
		
		$insert_array = array('toEmail' => $toEmail,'toName' => $userName,'fromEmail' =>FROM_EMAIL,'fromName' =>FROM_NAME,'replyTo' =>'','subject' =>$subject,
		'msgBody' =>$msgBody,'sentStatus' =>$sentStatus,'insertDateTime' =>date('Y-m-d H:i:s'));	
		$this->common_model_object->master_insert('email_notifications', $insert_array);
		
	}
	
 
	public function send_user_details($toEmail,$password,$userName,$link=''){
		$subject="Login Credentials of Inventory Portal";
		$this->data["title"]=$subject;
		$email_encoded = rtrim(strtr(base64_encode($toEmail), '+/', '-_'), '=');
		$mailcontent["userName"]=$userName;
		$mailcontent["login"]=$toEmail;
		$mailcontent["password"]=$password;
		$mailcontent["link"]=site_url(); 
		$this->data["mailcontent"]=$mailcontent;
		
		$this->data["content"]='mail/send_user_details';
		$msgBody = $this->CI->load->view('mail/layout_main',$this->data,TRUE);

		$sendquickmail = array('toEmail'=>$toEmail,'subject'=>$subject,'msgBody'=>$msgBody,'attachment'=>'');
		$sentStatus = sendquickmail($sendquickmail);	
		
		
		$insert_array = array('toEmail' => $toEmail,'toName' => $userName,'fromEmail' =>FROM_EMAIL,'fromName' =>FROM_NAME,'replyTo' =>'','subject' =>$subject,
		'msgBody' =>$msgBody,'sentStatus' =>$sentStatus,'insertDateTime' =>date('Y-m-d H:i:s'));	
		$this->common_model_object->master_insert('email_notifications',$insert_array);
	}	


public function logic_to_check_approval($param=array()){
		if((!empty($param)))
		{
			$approval_array = $this->CI->musers_approvals->read(array('userId'=>$param['userId'], 'fk_approvalTypeId'=>1),'result');
			if($approval_array)
			{
				$deptIds = array_column($approval_array, 'deptId'); 
				if (in_array($param['deptId'], $deptIds))
				{
					 return true;
				}
				else
				{
					return false;
				}
				 
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}

		
	public function logic_check_approval($param=array()){
		if((!empty($param)))
		{
			$approval_array = $this->CI->musers_approvals->read(array('userId'=>$param['userId'], 'fk_approvalTypeId'=>$param['fk_approvalTypeId']),'result');
			if($approval_array)
			{ 
				return true;
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}
		
	
	 
}
