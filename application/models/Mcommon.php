<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mcommon extends CI_Model{
	public function master_insert($array,$table_name){
		$this->db->insert($table_name,$array);
		$insert_id=$this->db->insert_id();
		return array('flag'=>1,'emsg'=>'','smsg'=>'Added','insert_id'=>$insert_id);
	}
	public function master_insert_batch($array_insert,$table_name){
		if($array_insert)
		{
			$this->db->insert_batch($table_name,$array_insert);	
		}
	}
	
	public function master_insert_batch_update_on_duplicate($table_name, $array_insert ) {
		if($table_name != '' &&  !empty($array_insert))
		{
			$this->db->on_duplicate($table_name, $array_insert);
		}
	}
	public function master_update_increament1($table_name='',$whereArr=array(),$field='',$count=''){
		if(!empty($whereArr)){
			foreach($whereArr as $whereArrKey => $whereArrVal){
				$this->db->where($whereArrKey,$whereArrVal);
			}
		} 
		$fc = $field.'+'.$count;
		$this->db->set($field, $fc, FALSE);
		$this->db->update($table_name);
	}
	
	public function master_update($table_name,$dataArr=array(),$whereArr=array()){
		if(!empty($whereArr)){
			foreach($whereArr as $whereArrKey => $whereArrVal){
				$this->db->where($whereArrKey,$whereArrVal);
			}
		}
		$this->db->update($table_name,$dataArr);
		unset($whereArr);
		unset($dataArr);
		return array('flag'=>1,'emsg'=>'','smsg'=>'Updated');
	}
	
	public function master_update_increament($table_name,$dataArr=array(),$whereArr=array()){
		
		/* 
			$this->db->where('id', $post['identifier']);
			$this->db->set('votes', 'votes+1', FALSE);
			$this->db->update('users');
		*/
		if(!empty($whereArr)){
			foreach($whereArr as $whereArrKey => $whereArrVal){
				$this->db->where($whereArrKey, $whereArrVal);
			}
		}
		if(!empty($dataArr)){
			foreach($dataArr as $dataArrKey => $dataArrVal){
				$this->db->set($dataArrKey, $dataArrVal, false);
			}
		} 
		
		$this->db->update($table_name);
		unset($whereArr); unset($dataArr);
		return array('flag'=>1,'emsg'=>'','smsg'=>'Updated');
	}
	
	

	public function master_update_batch($table_name,$dataArr=array(),$whereFeild=''){
		$this->db->update_batch($table_name,$dataArr, $whereFeild); 
		return array('flag'=>1,'emsg'=>'','smsg'=>'Updated');
	}
	public function master_delete($array,$table_name){
		
		$this->db->delete($table_name,$array);
	
		return array('flag'=>1,'emsg'=>'','smsg'=>'deleted');
	}
	public function select_field_result($table_name,$field_names,$whereArr=array()){
		$this->db->select($field_names);
		$this->db->from($table_name);
		if(!empty($whereArr)){
			foreach($whereArr as $whereArrKey => $whereArrVal){
				$this->db->where($whereArrKey,$whereArrVal);
			}
		}
		$query=$this->db->get();
		return $output=$query->result_array();	
	}
	public function select_field_result_single($table_name,$field_names,$whereArr=array()){
		$this->db->select($field_names);
		$this->db->from($table_name);
		if(!empty($whereArr)){
			foreach($whereArr as $whereArrKey => $whereArrVal){
				$this->db->where($whereArrKey,$whereArrVal);
			}
		}
		$query=$this->db->get();
		return $output=$query->row_array();	
	}
	
	
	
	
}

?>