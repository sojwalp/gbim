<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>ideamagix</title>
    <!-- plugins:css -->
    <link rel="stylesheet" href="<?php echo base_url()?>assets/vendors/iconfonts/mdi/css/materialdesignicons.min.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/vendors/iconfonts/ionicons/dist/css/ionicons.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/vendors/iconfonts/flag-icon-css/css/flag-icon.min.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/vendors/css/vendor.bundle.base.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/vendors/css/vendor.bundle.addons.css">
    <!-- endinject -->
    <!-- plugin css for this page -->
    <link rel="stylesheet" href="<?php echo base_url()?>assets/vendors/icheck/skins/all.css">
    <!-- End plugin css for this page -->
    <!-- inject:css -->
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/shared/style.css">
    <!-- endinject -->
    <!-- Layout styles -->
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/demo_1/style.css">
    <!-- End Layout styles -->
    <link rel="shortcut icon" href="<?php echo base_url()?>assets/images/favicon.ico" />
  </head>
  <body>
    <div class="container-scroller">
      <!-- partial:<?php echo base_url()?>partials/_navbar.html -->
      <nav class="navbar default-layout col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
        <div class="text-center navbar-brand-wrapper d-flex align-items-top justify-content-center">
          <a class="navbar-brand brand-logo" href="<?php echo base_url()?>index.html">
            <img src="<?php echo base_url()?>assets/images/logo.svg" alt="logo" /> </a>
          <a class="navbar-brand brand-logo-mini" href="<?php echo base_url()?>index.html">
            <img src="<?php echo base_url()?>assets/images/logo-mini.svg" alt="logo" /> </a>
        </div>
        <div class="navbar-menu-wrapper d-flex align-items-center">
            
          </button>
        </div>
      </nav>
      <!-- partial -->
      <div class="container-fluid page-body-wrapper">
        <!-- partial:<?php echo base_url()?>partials/_sidebar.html -->
        <nav class="sidebar sidebar-offcanvas" id="sidebar">
          <ul class="nav">
            <li class="nav-item nav-profile">
              <a href="#" class="nav-link">
                <div class="profile-image">
                  <img class="img-xs rounded-circle" src="<?php echo base_url()?>assets/images/faces/face8.jpg" alt="profile image">
                  <div class="dot-indicator bg-success"></div>
                </div>
                <div class="text-wrapper">
                  <p class="profile-name"><?php $sessdata= $this->session->userdata('session_array'); ?></p>
                  Welcome <?php echo $sessdata['username']; ?>
                   <a href="<?php echo base_url('adminlogin/logout'); ?>">Logout</a>
                </div>
              </a>
            </li>
            <li class="nav-item nav-category">Main Menu</li>
            <li class="nav-item">
              <a class="nav-link" href="<?php echo base_url()?>index.html">
                <i class="menu-icon typcn typcn-document-text"></i>
                <span class="menu-title">Dashboard</span>
              </a>
            </li>
            
            <li class="nav-item">
              <a class="nav-link" href="<?php echo base_url()?>adminlogin/show_list_user">
                <i class="menu-icon typcn typcn-shopping-bag"></i>
                <span class="menu-title">List User</span>
              </a>
            </li>
            
          </ul>
        </nav>
        <!-- partial -->
        <div class="main-panel">
          <div class="content-wrapper">
            <div class="row">

              <div class="col-md-12 grid-margin stretch-card">
                <div class="card">
                  <div class="card-body">
                    <h4 class="card-title">Add Instructor</h4>
                    <form class="common_function" enctype="multipart/form-data" method="post" action="<?php echo base_url()?>adminlogin/add_instructor">
                    
                     <input type='hidden' name='input_error_classes' value='intructname,instructdesc,img,level'>
                  <input type='hidden' name='error_class' value='emsg'>
                  <input type='hidden' name='success_class' value='smsg'>


                      <div class="form-group">
                        <label for="exampleInputPassword4"> Instruct Name</label>
                        <input type="text" class="form-control" id="exampleInputPassword4" name="intructname" placeholder="Name">
                      </div>
                      <div class="form-group">
                        <label>File upload</label>
                        <div class="input-group col-xs-12">
                          <input type="file" class="form-control" placeholder="Upload Image">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="exampleInputCity1">level</label>
                        <select name="level" class="form-control">
                          <option value="0"> Select Level</option>
                          <option value="1">1</option>
                          <option value="2">2</option>
                          <option value="3">3</option>
                        </select>

                      </div>
                      <div class="form-group">
                        <label for="exampleTextarea1">Add Instructor</label>
                        <textarea class="form-control" id="exampleTextarea1" name="instructdesc" maxlength="100" rows="2"></textarea>
                        <p class="text-danger "></p>
                      </div>



                      <button type="submit" class="btn btn-success mr-2">Submit</button>
                      <button class="btn btn-light" type="reset">Cancel</button>

                       <p class='text-center'>
                      <span class="text-danger emsg"></span>
                      <span class="text-success smsg"></span>
                      </p>

                    </form>
                  </div>
                </div>
              </div>
              
            </div>
          </div>
          <!-- content-wrapper ends -->
          <!-- partial:<?php echo base_url()?>partials/_footer.html -->
          <footer class="footer">
            <div class="container-fluid clearfix">
              <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © 2019 <a href="http://www.bootstrapdash.com/" target="_blank">Bootstrapdash</a>. All rights reserved.</span>
              <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Hand-crafted & made with <i class="mdi mdi-heart text-danger"></i>
              </span>
            </div>
          </footer>
          <!-- partial -->
        </div>
        <!-- main-panel ends -->
      </div>
      <!-- page-body-wrapper ends -->
    </div>
    <!-- container-scroller -->
    <!-- plugins:js -->
    <script src="<?php echo base_url()?>assets/vendors/js/vendor.bundle.base.js"></script>
    <script src="<?php echo base_url()?>assets/vendors/js/vendor.bundle.addons.js"></script>
    <!-- endinject -->
    <!-- Plugin js for this page-->
    <!-- End plugin js for this page-->
    <!-- inject:js -->
    <script src="<?php echo base_url()?>assets/js/shared/off-canvas.js"></script>
    <script src="<?php echo base_url()?>assets/js/shared/misc.js"></script>
    <script src="<?php echo base_url()?>assets/js/common.js"></script>

    <!-- endinject -->
    <!-- Custom js for this page-->
    <!-- End custom js for this page-->
  </body>
</html>