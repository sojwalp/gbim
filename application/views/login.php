
<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>GBIM</title>
    <!-- plugins:css -->
    <link rel="stylesheet" href="<?php echo base_url()?>assets/vendors/iconfonts/mdi/css/materialdesignicons.min.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/vendors/iconfonts/ionicons/dist/css/ionicons.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/vendors/iconfonts/flag-icon-css/css/flag-icon.min.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/vendors/css/vendor.bundle.base.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/vendors/css/vendor.bundle.addons.css">
    <!-- endinject -->
    <!-- plugin css for this page -->
    <!-- End plugin css for this page -->
    <!-- inject:css -->
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/shared/style.css">
    <!-- endinject -->
    <link rel="shortcut icon" href="<?php echo base_url()?>assets/images/favicon.ico" />
  </head>
  <body>
    <div class="container-scroller">
      <div class="container-fluid page-body-wrapper full-page-wrapper">
        <div class="content-wrapper d-flex align-items-center auth auth-bg-1 theme-one">
          <div class="row w-100">
            <div class="col-lg-4 mx-auto">
              <div class="auto-form-wrapper">
                <form class="common_function" method="post" action="<?php echo base_url(); ?>adminlogin" >

                  <input type='hidden' name='input_error_classes' value='emailId,mobileno,firstName,lastName,password,fk_roletypeId'>
                  <input type='hidden' name='error_class' value='emsg'>
                  <input type='hidden' name='success_class' value='smsg'>


                  <div class="form-group">
                    <label class="label">first Name</label>
                    <div class="input-group">
                      <input type="text" class="form-control" name="firstName" placeholder="firstName">
                      <div class="input-group-append">
                        <span class="input-group-text">
                          <i class="mdi mdi-check-circle-outline"></i>
                        </span>
                        
                      </div>
                      <p class="text-danger firstName"></p>
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="label">Last Name</label>
                    <div class="input-group">
                      <input type="text" class="form-control" name="lastName" placeholder="lastName">
                      <div class="input-group-append">
                        <span class="input-group-text">
                          <i class="mdi mdi-check-circle-outline"></i>
                        </span>
                        
                      </div>
                      <p class="text-danger lastName"></p>
                    </div>
                  </div>


                  <div class="form-group">
                    <label class="label">email id</label>
                    <div class="input-group">
                      <input type="text" class="form-control" name="emailId" placeholder="email Id">
                      <div class="input-group-append">
                        <span class="input-group-text">
                          <i class="mdi mdi-check-circle-outline"></i>
                        </span>
                        
                      </div>
                      <p class="text-danger emailId"></p>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="label">Mobile No</label>
                    <div class="input-group">
                      <input type="text" name="mobileno" class="form-control" maxlength="10" placeholder="Ex:9000090090">
                      <div class="input-group-append">
                        <span class="input-group-text">
                          <i class="mdi mdi-check-circle-outline"></i>
                        </span>

                      </div>
                       <p class="text-danger mobileno"></p>

                    </div>
                  </div>
                  <div class="form-group">
                    <label class="label">password</label>
                    <div class="input-group">
                      <input type="password" class="form-control" name="password" placeholder="password">
                      <div class="input-group-append">
                        <span class="input-group-text">
                          <i class="mdi mdi-check-circle-outline"></i>
                        </span>
                        
                      </div>
                      <p class="text-danger password"></p>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="label">Select Type</label>
                    <div class="input-group">
                        <select name="fk_roletypeId" class="form-control">
                          <option value="">Select Type</option>
                          <?php 
                          if($type){
                            foreach ($type as  $value) {
                            
                          ?>
                          <option value="<?php echo $value['mroletypeId']; ?>"><?php echo $value['typeName']; ?></option>
                          <?php 
                             }
                            }
                         ?>
                        </select>
                        <div class="input-group-append">
                        <span class="input-group-text">
                          <i class="mdi mdi-check-circle-outline"></i>
                        </span>
                        
                      </div>
                      <p class="text-danger fk_roletypeId"></p>
                    </div>
                  </div>
                  
                  <div class="form-group">
                    <button type="submit" class="btn btn-primary submit-btn btn-block">Generate Token</button>
                    <p class='text-center'>
                      <span class="text-danger emsg"></span>
                      <span class="text-success smsg"></span>
                    </p>

                  </div>
                 
                </form>
              </div>
             
            </div>
          </div>
        </div>
        <!-- content-wrapper ends -->
      </div>
      <!-- page-body-wrapper ends -->
    </div>
    <!-- container-scroller -->
    <!-- plugins:js -->
    <script src="<?php echo base_url()?>assets/vendors/js/vendor.bundle.base.js"></script>
    <script src="<?php echo base_url()?>assets/vendors/js/vendor.bundle.addons.js"></script>
    <!-- endinject -->
    <!-- inject:js -->
    <script src="<?php echo base_url()?>assets/js/shared/off-canvas.js"></script>
    <script src="<?php echo base_url()?>assets/js/shared/misc.js"></script>
    <script src="<?php echo base_url()?>assets/js/common.js"></script>
    <!-- endinject -->
  </body>
</html>