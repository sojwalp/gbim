
//common function to submit form
$(document).ready(function () {
	$("body").on("submit",".common_function",function(e){
		that=$(this);
		// ShowLoading_form_common(that);
		e.preventDefault();
		//return false;
		input_error_classes=$(this).find('input[name="input_error_classes"]').val();
		error_class=$(this).find('input[name="error_class"]').val();
		success_class=$(this).find('input[name="success_class"]').val();
		var input_error = input_error_classes.split(',');
		var i;
		for (i = 0; i < input_error.length; ++i) {
			$("."+input_error[i]).text('');
		}	
		$("."+error_class).text('');
		$("."+success_class).text('');
		$.ajax({
			url : $(this).attr('action') || window.location.pathname,
			type: $(this).attr('method'),
			data: $(this).serialize(),
			dataType: 'json',
			success: function (data) {
				
			
				if(data['flag']==3)
				{
					var input_error = input_error_classes.split(',');
					var i;
					for (i = 0; i < input_error.length; ++i) {
						that.find("."+input_error[i]).html(data[input_error[i]]);
					}	
					that.find("."+error_class).html(data['emsg']);
					// removeLoading_form();
				}
				if(data['flag']==2)
				{
					that.find("."+error_class).html(data['emsg']);
					// removeLoading_form();
				}
				if(data['flag']==1)
				{
					
					if(data['redirect'])
					{
						//removeLoading_form();
						that.find("."+success_class).html(data['smsg']);
						that.find("."+error_class).html(data['emsg']);
						window.setTimeout(function(){
							if(data['redirect']==1)
							{
								location.reload();
							}
							else
							{
								window.location.replace(data['redirect']);
							}
							
						}, 500);	
					}
					else
					{
						// removeLoading_form();
						
						that.find("."+success_class).html(data['smsg']);
						setTimeout(function(){that.find("."+success_class).html(''); }, 1000);
					}
				}
			},
			error: function (jXHR, textStatus, errorThrown) {
				// removeLoading_form();
				that.find("."+error_class).html(errorThrown);
			}
		});
    });
});

//common function to submit form
$(document).ready(function () {
	$("body").on("submit","#form_add",function(e){
		that=$(this).attr('id');
		ShowLoading_form(that);
		e.preventDefault();
		
		input_error_classes=$(this).find('input[name="input_error_classes"]').val();
		error_class=$(this).find('input[name="error_class"]').val();
		success_class=$(this).find('input[name="success_class"]').val();
		var input_error = input_error_classes.split(',');
		var i;
		for (i = 0; i < input_error.length; ++i) {
			$("."+input_error[i]).text('');
		}	
		$("."+error_class).text('');
		$("."+success_class).text('');
		$.ajax({
			url : $(this).attr('action') || window.location.pathname,
			type: $(this).attr('method'),
			data: $(this).serialize(),
			dataType: 'json',
			success: function (data) {
				removeLoading_form();
			
				if(data['flag']==3)
				{
					var input_error = input_error_classes.split(',');
					var i;
					for (i = 0; i < input_error.length; ++i) {
						$("."+input_error[i]).text(data[input_error[i]]);
					}	
					$("."+error_class).text(data['emsg']);
				}
				if(data['flag']==2)
				{
					$("."+error_class).text(data['emsg']);
				}
				if(data['flag']==1)
				{
					if(data['redirect'])
					{
						$("."+success_class).html(data['smsg']+" ...");
						window.setTimeout(function(){
							if(data['redirect']==1)
							{
								location.reload();
							}
							else
							{
								window.location.replace(data['redirect']);
							}
							
						}, 500);	
					}
					else
					{
						$("."+success_class).text(data['smsg']);
					}
				}
			},
			error: function (jXHR, textStatus, errorThrown) {
				removeLoading_form();
				$("."+error_class).html(errorThrown);
			}
		});
    });
});

$(document).ready(function () {
  //called when key is pressed in textbox
   $("body").on("keypress",".onlynumberss",function(e){
     //if the letter is not digit then display error and don't type anything
     if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        //display error message
        alert("Digits Only");
		return false;
    }
   });
});
$(document).ready(function () {
  //called when key is pressed in textbox
   $("body").on("keypress",".onlynumbers",function(e){
	   $(this).siblings(".txtboxToFilteremsg").text('');
     //if the letter is not digit then display error and don't type anything
     if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        //display error message
        if($(this).siblings(".txtboxToFilteremsg").length)
		{
			$(this).siblings(".txtboxToFilteremsg").text('Only numbers');
		}
		else
		{
			$("<small class='text-danger txtboxToFilteremsg'>Only numbers</small>").insertAfter($(this));
		}
		return false;
    }
   });
});

 

